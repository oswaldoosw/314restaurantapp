============
Requirement:
============
- You need to have node.js installed in your system
- You need to have XAMPP installed in your system


=======
To Run:
=======
- Open XAMPP Control Panel
- Press Start button for Apache and MySQL Module
- Press Admin button for MySQL or go to "http://localhost/phpmyadmin/"
- Create a new database called "restaurantapp"
- Import the restaurantapp.sql file to the database under database folder(restaurantapp_csit314/database/restaurantapp.sql)

- Download the restaurantapp_csit314 folder and unzip it
- Open Terminal
- Using cd command, navigate to restaurantapp_csit314 folder

For Example: >cd Downloads/restaurantapp_csit314

- Type: >node index.js
- Open your browser and go to "http://localhost:3000/"
- The login page will be shown


==============
Login Details:
==============

User Admin
username: useradmin1
password: useradmin123

Manager
username: manager1
password: manager123

Staff
username: staff1
password: staff123

Customer
username: customer1
password: customer123

Restaurant Owner
username: owner1
password: owner123


=====
Note:
=====
- The test script for TDD is under test_scripts folder (restaurantapp_csit314/test_scripts/)
- The script file for 100 records is under restaurantapp_csit314 folder (restaurantapp_csit314/user100.js)

